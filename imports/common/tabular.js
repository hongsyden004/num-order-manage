import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
// import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { Partners } from '../collections/partner.js';
import { Province } from '../collections/province';
import { Category } from '../collections/category.js';
import { Unit } from '../collections/unit.js';
import { Items } from '../collections/items.js';

export const TabularTables = {};

TabularTables.MemberListTable = new Tabular.Table({
    name: "MemberListTable",
    collection: Partners,
    responsive: true,
    bFilter: false,
    stateSave: false,
    autoWidth: false,
    selector(userId) {
        return {}
    },
    columns: [
        { data: "name", title: "Name" },
        { data: "email", title: "Email" },
        // {
        //     tmpl: Meteor.isClient && Template.admin_member_list
        // }
    ]
});

TabularTables.Province = new Tabular.Table({
    name: "Province",
    collection: Province,
    responsive: true,
    bFilter: true,
    stateSave: false,
    autoWidth: false,
    columns: [
        { data: "name", title: "Name" },
        { data: "view_order", title: "View order" },
        {
            data: "_id",
            title: "Edit",
            render: function(e) {
                return Spacebars.SafeString('<a href="#" id="edit_province" data-value=' + e + ' class="btn btn-primary fa fa-pencil btn-sm" style="cursor: pointer;"> Edit</a>');
            }
        },
        {
            data: "_id",
            title: "Delete",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="delete_province" data-id=' + e + ' class="btn btn-danger fa fa-trash btn-sm" style="cursor: pointer;"> Delete</a>');
            }
        }

    ]

});

TabularTables.Category = new Tabular.Table({
    name: "Category",
    collection: Category,
    responsive: true,
    bFilter: true,
    stateSave: false,
    autoWidth: false,
    columns: [
        { data: "name", title: "Name" },
        { data: "view_order", title: "View order" },
        {
            data: "isview",
            title: "Status",
            render: function(type, e, doc) {
                if(doc.isview) {
                    return ' <span class="badge bg-success" id="status_change" data-id=' + doc._id + '>Publish</span> ';
                } else {
                    return '<span class="badge bg-warning" id="status_change" data-id=' + doc._id + '>Unpublish</span> ';
                }
            }
        },
        {
            data: "_id",
            title: "Edit",
            render: function(e) {
                return Spacebars.SafeString('<a href="#" id="edit_category" data-value=' + e + ' class="btn btn-primary fa fa-pencil btn-sm" style="cursor: pointer;"> Edit</a>');
            }
        },
        {
            data: "_id",
            title: "Delete",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="delete_category" data-id=' + e + ' class="btn btn-danger fa fa-trash btn-sm" style="cursor: pointer;"> Trash</a>');
            }
        },
        {
            data: "_id",
            title: "Action",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="view_category" data-id=' + e + ' class="btn btn-info btn-sm" style="cursor: pointer;"> <i class="fas fa-eye"></i> Detail</a>');
            }
        }

    ]

});

TabularTables.Unit = new Tabular.Table({
    name: "Unit",
    collection: Unit,
    responsive: true,
    bFilter: true,
    stateSave: false,
    autoWidth: false,
    columns: [
        { data: "name", title: "Name" },
        { data: "view_order", title: "View order" },
        {
            data: "_id",
            title: "Edit",
            render: function(e) {
                return Spacebars.SafeString('<a href="#" id="edit_unit" data-id=' + e + ' class="btn btn-primary fa fa-pencil btn-sm" style="cursor: pointer;"> Edit</a>');
            }
        },
        {
            data: "_id",
            title: "Delete",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="delete_unit" data-id=' + e + ' class="btn btn-danger fa fa-trash btn-sm" style="cursor: pointer;"> Delete</a>');
            }
        }
    ]
});

TabularTables.ItemsList = new Tabular.Table({
    name: "ItemsList",
    collection: Items,
    responsive: true,
    bFilter: true,
    stateSave: false,
    autoWidth: false,
    columns: [
        { data: "title", title: "Title Item" },
        { data: "itemcode", title: "Code Item" },
        { 
            data: "category_id", 
            title: "Category",
            render: function(e) {
                var rsCate = Category.findOne({_id: e});
                if(rsCate !== undefined) return rsCate.name;
            }
        },
        { 
            data: "unit_id", 
            title: "Unit",
            render: function(e) {
                var rsUnit = Unit.findOne({_id: e});
                if(rsUnit !== undefined) return rsUnit.name;
            }
        },
        { 
            data: "price", 
            title: "Price",
            render: function(e) {
                return '$ '+e;
            }
        },
        {
            data: "_id",
            title: "Edit",
            render: function(e) {
                return Spacebars.SafeString('<a href="#" id="edit_item" data-id=' + e + ' class="btn btn-primary fa fa-pencil btn-sm" style="cursor: pointer;"> Edit</a>');
            }
        },
        {
            data: "_id",
            title: "Delete",
            render: function(e) {
                console.log(e);
                return Spacebars.SafeString('<a href="#" id="delete_item" data-id=' + e + ' class="btn btn-danger fa fa-trash btn-sm" style="cursor: pointer;"> Delete</a>');
            }
        }
    ]
});