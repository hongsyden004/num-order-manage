// AccountsTemplates.configure({
//     confirmPassword: false,
//     enablePasswordChange: false,
//     forbidClientAccountCreation: false,
//     hideSignUpLink: true,
//     hideSignInLink: true,
//     //overrideLoginErrors: true,
//     enforceEmailVerification: false,
//     sendVerificationEmail: false,
//     showAddRemoveServices: false,
//     showForgotPasswordLink: false,
//     showLabels: true,
//     showPlaceholders: false,
//     showResendVerificationEmailLink: false,

//     texts: {
//         title: {
//             signIn: "សូមសរសេរលេខគណនី និងពាក្យសំងាត់ខាងក្រោម៖",
//             signUp: "ចុះឈ្មោះប្រើប្រាស់សេវាកម្មនេះ",
//         },
//         button: {
//             signIn: "ចូលគណនី",
//             signUp: "ចុះឈ្មោះ",
//         },
//         errors: {
//             loginForbidden: "លេខសំងាត់មិនត្រឹមត្រូវ!",
//             mustBeLoggedIn: "លោកអ្នកចាំបាច់ត្រូវពិនិត្យចូលជាមុនសិន!",
//         }
//     }

// });

// AccountsTemplates.removeField('email');
// var pwd = AccountsTemplates.removeField('password');

// AccountsTemplates.addFields([
//     {
//         _id: "username",
//         type: "text",
//         displayName: "ទូរស័ព្ទ",
//         required: true,
//         re: /^(\d+){9,10}$/,
//         errStr: 'មិនត្រឹមត្រូវ'
//     },
//     {
//         _id: 'password',
//         type: 'password',
//         displayName: "លេខសំងាត់",
//         required: true,
//         minLength: 8
//     }
// ]);

AccountsTemplates.configure({
    defaultLayout: 'myaccount',
});

AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp', {
    redirect: '/profile',
});