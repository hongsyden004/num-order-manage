import { Partners } from "../collections/partner";

Router.plugin('ensureSignedIn', {
    except: ['public', 'atSignIn', 'atSignUp', 'atForgotPassword', 'aboutus_route']
});

Router.route('/logout', function() {
    Meteor.logout(function(err) {
        if (!err) Router.go('/');
    });
});

Router.route('/', {
    loadingTemplate: 'loading',
    waitOn: function() {
        return Meteor.subscribe('signedinPartner');
    },
    name: 'public'
});

Router.route('/profile', function() {
    this.render('profile');
}, { name: 'profile' });


//===================================
// Admin Router
//===================================
Router.route('/admin/member/list', function() {
    this.render('admin_member_list');
}, { name: 'admin_member_list' });

Router.route('/admin/province/record', function() {
    this.render('province_record');
}, { name: 'province_record' });

Router.route('/admin/category/record', function() {
    this.render('category_record');
}, { name: 'category_record' });

Router.route('/aboutus', function() {
    this.render('aboutus_template');
}, {name: 'aboutus_route'} )

Router.route('/admin/unit/record', function() {
    this.render('unit_record');
})

Router.route('/record/items', function(){
    this.render('items_record')
})
//===================================
// End Admin Router
//===================================