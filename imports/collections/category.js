import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
export const Category = new Mongo.Collection('Category');

const Schemas = {};

Schemas.Category = new SimpleSchema({
    name: {
        type: String,
    },
    view_order: {
        type: Number,
    },
    isview: {
        type: Boolean, // Ture Or False
        defaultValue: true
    },
    createdAt: {
        type: Date,
        label: 'Date',
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    }
}, { tracker: Tracker });

Category.attachSchema(Schemas.Category);

if (Meteor.isServer) 
{
    Category.allow({
        insert: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        update: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        remove: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        }
    });

    Category.after.insert(function (userId, doc) {
        
    });
    
    Category.before.insert(function (userId, doc) {
        
    });
}