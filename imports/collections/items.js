import SimpleSchema from 'simpl-schema';
import { Category } from './category';
import { Unit } from './unit';
SimpleSchema.extendOptions(['autoform']);
export const Items = new Mongo.Collection('Items');

const Schemas = {};

Schemas.Items = new SimpleSchema({
    itemcode: {
        type: String, // DEN-001
        label: 'Item Code',
    },
    title: {
        type: String
    },
    category_id: {
        type: String,
        label: 'Category',
        autoform: {
            type: 'select',
            options: () => {
                var resultCat = Category.find({}).fetch();
                var optionData = [];
                resultCat.forEach(val => {
                    optionData.push({label: val.name, value: val._id});
                });
                return optionData;
            }
        }
    },
    unit_id: {
        type: String,
        label: 'Unit',
        autoform: {
            type: 'select',
            options: () => {
                var resultUnit = Unit.find({}).fetch();
                var optionData = [];
                resultUnit.forEach(val => {
                    optionData.push({label: val.name, value: val._id});
                });
                return optionData;
            }
        }
    },
    description: {
        type: String,
        optional: true,
        autoform: {
            type: 'textarea',
            rows: 2
        }
    },
    price: {
        type: Number,
        // decimal: true
    },
    image: {
        type: String,
        optional: true
    },
    created_at: {
        type: Date,
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    }
}, {tracker: Tracker});

Items.attachSchema(Schemas.Items);

if(Meteor.isServer) {
    Items.allow({ 
        insert: function() { 
            return true; 
        }, 
        update: function() { 
            return true; 
        }, 
        remove: function() { 
            return true; 
        } 
    });
    
    
}