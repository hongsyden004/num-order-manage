import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
export const Unit = new Mongo.Collection('Unit');

const Schemas = {};

Schemas.Unit = new SimpleSchema({
    name: {
        type: String,
    },
    view_order: {
        type: Number,
    },
    createdAt: {
        type: Date,
        label: 'Date',
        autoValue: function() {
            if (this.isInsert) {
                return new Date();
            }
        }
    }
}, { tracker: Tracker });

Unit.attachSchema(Schemas.Unit);

if (Meteor.isServer) 
{
    Unit.allow({
        insert: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        update: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        },
        remove: function() {
            if (Roles.userIsInRole(Meteor.userId(), ['admin'])) return true;
            return false;
        }
    });

    Unit.after.insert(function (userId, doc) {
        
    });
    
    Unit.before.insert(function (userId, doc) {
        
    });
}