import { Unit } from '../../imports/collections/unit'
import Swal from 'sweetalert2';
import './unit_record.html'

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.unit_record.helpers({
    CollectionName: () => {
        return Unit;
    },
    getUnit: () => {
        var id = Session.get('unitId');
        return Unit.findOne({_id: id});
    },
    formType: () => {
        if(Session.get('unitId')) {
            return 'update';
        } else {
            return 'insert';
        }
    }
})

Template.unit_record.events({
    'click #delete_unit': (e) => {
        var id = e.currentTarget.dataset.id;
        Meteor.call('DeleteUnit', id);
    },
    'click #edit_unit': (e) => {
        var id = e.currentTarget.dataset.id;
        console.log(id);

        Session.set('unitId', id);
    },
})

AutoForm.addHooks(['Unit'], {
    onSuccess(type, rs) {
        Toast.fire({
            icon: 'success',
            title: 'Successfully.'
        });
        Session.set('unitId', undefined);
    },
    onError(type, err) {
        Toast.fire({
            icon: 'error',
            title: 'Something wwnt wrong.'
        })
    },
});