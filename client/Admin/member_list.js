import './member_list.html';
import { Template } from 'meteor/templating';
import { TabularTables } from '../../imports/common/tabular.js';

Template.admin_member_list.helpers({
    MemberList: function(event) {
        console.log('====================================');
        console.log(TabularTables.MemberListTable);
        console.log('====================================');
        return TabularTables.MemberListTable;
    }
});