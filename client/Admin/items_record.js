import { Items } from '../../imports/collections/items';
import './items_record.html';
import Swal from 'sweetalert2';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.items_record.helpers({
    CollectionItems: () => {
        return Items;
    },
    manageType: () => {
        if(Session.get('ItemID')){
            return 'update';
        }

        return 'insert';
    },
    isUpdate: () => {
        return Session.get('ItemID');
    },
    ItemsDoc: () => {
        var itemId = Session.get('ItemID');
        Meteor.subscribe('getItemById', itemId);
        var resultItem = Items.findOne({_id: itemId});
        console.log("resultItem");
        console.log(resultItem);

        return resultItem;
    }
})

Template.items_record.events({
    'click #edit_item': (e) => {
        console.log(e.currentTarget.dataset.id);
        Session.set('ItemID', e.currentTarget.dataset.id);
    },
    'click #btn-cancel': (e) => {
        Session.set('ItemID', undefined);
    },
    'click #delete_item': (e) => {
        console.log(e.currentTarget.dataset.id);

        var itemId = e.currentTarget.dataset.id;
        if(itemId)
        {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-sm btn-margin-left',
                    cancelButton: 'btn btn-danger btn-sm btn-margin-left'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Confirmation!',
                text: "Are you sure to delete?",
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    Meteor.call('DeleteItem', itemId, (err, rs) => {
                        if (rs) {
                            Toast.fire({
                                icon: 'success',
                                title: 'Delete Successfully.'
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'Delete not Successfully.'
                            })
                        }
                    });
                }
            })
        }
    }
})

AutoForm.hooks({
    ItemsRecord: {
        before: {
            insert: function(doc) {
                setTimeout(() => {
                    $('#btn-save').attr('disabled', false)
                }, 0);

                if (!AutoForm.validateForm(this.formId)) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Something went wrong !!!!!!!.'
                    })
                    return this.result(doc); // continue with normal submission which will fail due to validation
                }
                this.resetForm(this.formId);
                Toast.fire({
                    icon: 'success',
                    title: 'Successfully.'
                });
                return this.result(doc);
            },
        },
    }
});

AutoForm.addHooks(['ItemsRecord'], {
    onSuccess(type, rs) {
        Toast.fire({
            icon: 'success',
            title: 'Successfully.'
        });
        $('#btn-save').prop('disabled', false);
        Session.set('ItemID', undefined);
    },
    onError(type, err) {
        Toast.fire({
            icon: 'error',
            title: 'Something wwnt wrong.'
        })
    },
});