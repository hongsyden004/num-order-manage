import { Template } from 'meteor/templating';
import { AutoForm } from 'meteor/aldeed:autoform';
import { Category } from '../../imports/collections/category.js';
import Swal from 'sweetalert2';
import './category_record.html';
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.category_record.helpers({
    CollectionOne: () => {
        return Category;
    },
    manageType: () => {
        var id = Session.get('edit_category');
        if (id !== undefined) {
            return 'update';
        } else {
            return 'insert';
        }
    },
    categoryDoc: () => {
        var id = Session.get('edit_category');
        Meteor.subscribe('getCategoryById', id);
        var rs = Category.findOne({ _id: id });
        if (rs !== undefined) return rs;
    },
});
Template.category_record.events({
    'click #delete_category': function(event){
        var value = event.currentTarget.dataset;
		// delete on server
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 1500,
		})
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success btn-sm btn-margin-left',
				cancelButton: 'btn btn-danger btn-sm btn-margin-left'
			},
			buttonsStyling: false
		})
		swalWithBootstrapButtons.fire({
			title: 'Confirmation!',
			text: "Are you sure to delete?",
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				Meteor.call('DeleteCategory', value.id, (err, rs) => {
					if (rs) {
						Toast.fire({
							icon: 'success',
							title: 'Delete Successfully.'
						})
					} else {
						Toast.fire({
							icon: 'error',
							title: 'Delete not Successfully.'
						})
					}
				});
			}
		})
    },
    'click #edit_category': function(event) {
        var dt = event.currentTarget.dataset;
        if(dt.value !== undefined);
        {
            Session.set('edit_category', dt.value);
        }
    },
    'click #status_change': function(event) {
        var value = event.currentTarget.dataset;
        Meteor.call('updateIsView', value.id);
    }
});

AutoForm.hooks({
    Category: {
        before: {
            insert: function(doc) {
                setTimeout(() => {
                    $('#btn-save').attr('disabled', false)
                }, 0);

                if (!AutoForm.validateForm(this.formId)) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Something went wrong.'
                    })
                    return this.result(doc); // continue with normal submission which will fail due to validation
                }
                this.resetForm(this.formId);
                Toast.fire({
                    icon: 'success',
                    title: 'Successfully.'
                });
                return this.result(doc);
            },
        },
    }
});

AutoForm.addHooks(['Category'], {
    onSuccess(type, rs) {
        Toast.fire({
            icon: 'success',
            title: 'Successfully.'
        });
        $('#btn-save').prop('disabled', false);
        Session.set('edit_category', undefined);
    },
    onError(type, err) {
        Toast.fire({
            icon: 'error',
            title: 'Something wwnt wrong.'
        })
    },
});
