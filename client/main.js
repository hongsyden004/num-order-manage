import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Roles } from 'meteor/alanning:roles';

import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs';
// import 'datatables.net-bs/css/dataTables.bootstrap.css';
dataTablesBootstrap(window, $);

Meteor.subscribe('getAllCategory');
Meteor.subscribe('getAllUnit');

import './main.html';
import './homepage';
import './aboutus'

import 'croppie/croppie.js';
import 'croppie/croppie.css';

import './profile/profile.html';
import './profile/profile.js';
//post

//===================================
// Admin Import
//===================================
import './Admin/member_list';
import './Admin/province_record';
import './Admin/category_record';
import './Admin/unit_record';
import './Admin/items_record';

//===================================
// End Admin Import
//===================================

import './profile/photo_profile.js';
import './profile/photo_profile.html';

import '../imports/common/router.js';
import '../imports/common/user.js';
import '../imports/common/tabular.js';

import { Partners } from '/imports/collections/partner.js';
import Common from '../imports/common/common';

import Swal from 'sweetalert2';

const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
    })
    // 11:01 AM | June 9
Template.registerHelper('formatDateChat', function(sdate) {
    if (sdate) return moment(sdate).format("hh:mm | MMM DD");
});

Template.registerHelper('FormatMMDD', function(sdate) {
    if (sdate) return moment(sdate).format("MMM DD");
});

Template.registerHelper('FormatDate', function(sdate) {
    if (sdate) return moment(sdate).format("DD MMMM YYYY");
});

Template.registerHelper('FormatYYYY', function(sdate) {
    if (sdate) return moment(sdate).format("YYYY");
});

Template.registerHelper('NamePartner', function(id) {
    if (id) {
        Meteor.subscribe('PublishPartnerById', id);
        let result = Partners.findOne({ _id: id });
        if (result !== undefined) return result.name;
    }
});

Template.registerHelper('verifypartner', function(partnerid) {
    if (partnerid !== '') {
        Meteor.subscribe('PublishPartnerById', partnerid);
        let result = Partners.findOne({ _id: partnerid });
        if (result !== undefined) {
            if (result.verify == true) return new Spacebars.SafeString('<i class="fa fa-check-circle verify-icon" aria-hidden="true"></i>');
        }
    }
});

//publish all
Meteor.subscribe('PublishUserLogin', Meteor.userId());

Template.registerHelper('userLogin', function(id) {
    if (Meteor.userId() !== null) return true;
    return false;
});

Template.navbar.helpers({
    active: function(path) {
        if (Router.current().route !== undefined) {
            var routename = Router.current().route.getName();
            var rsArray = path.split(',');
            var rsCheck = rsArray.includes(routename);
            if (rsCheck) {
                return "active";
            }
        }
    },
    checkName: () => {
        return Router.current().route.getName();
    }
})

Template.navbar.events({
    'click .animation': (event) => {
        window.scroll({
            top: 0,
            behavior: 'smooth'
        });
        // $("html, body").animate({ scrollTop:0 }, 500);
    },
    'keyup #text-message': (event) => {
        if (event.target.value !== '') {
            $('.btn-send-comment').removeClass('disabled');
        } else {
            $('.btn-send-comment').addClass('disabled');
        }

        let value = $('#text-message').attr('rows');
        if (event.keyCode === 13) {
            $('#text-message').attr('rows', parseFloat(value) + 1);
        }
    },
    'click .btn-send-comment': (event) => {
        let value = $('#text-message').val();
        if (value !== '') {
            Meteor.call('CommentMethod', value, Router.current().params.id, (err, rs) => {
                if (rs !== '' && rs !== undefined) {
                    $('#text-message').val('');
                    $('#text-message').attr('rows', 1);
                    Toast.fire({
                        icon: 'success',
                        title: 'Comment Successfully.'
                    })
                }
            })
        }
        $('.hidden-by-focus').removeClass('hidden');
        $('.comment-bar').addClass('class-for-check');
    },
    'focus #text-message': (event) => {
        $('.hidden-by-focus').addClass('hidden');
        $('.comment-bar').removeClass('class-for-check');
    },
    'focusout #text-message': (event) => {
        $('.hidden-by-focus').removeClass('hidden');
        $('.comment-bar').addClass('class-for-check');
    },
})


Template.myaccount.helpers({
    accountstate: function() {
        var state = AccountsTemplates.getState();
        return AccountsTemplates.getState();
    }
})
Template.myaccount.events({
    'click #employee': function() {
        $("#at-field-role").val($("input[name='urole']:checked").val());
        $('#at-btn').prop("type", "submit");
        localStorage.setItem("mkit_role_select", $("input[name='urole']:checked").val());
    },
    'click #employer': function() {
        $("#at-field-role").val($("input[name='urole']:checked").val());
        $('#at-btn').prop("type", "submit");
        localStorage.setItem("mkit_role_select", $("input[name='urole']:checked").val());
    },
})

Accounts.onLogin(function() {
    var checkrole = Roles.userIsInRole(Meteor.userId(), ['admin', 'employee', 'employer']);
    if (checkrole === false) {
        Meteor.call("setRoleForExternal", localStorage.getItem("mkit_role_select")); //user
        localStorage.removeItem("mkit_role _select");
    }
});

if (Meteor.isClient) {
    Meteor.startup(function() {
        
    });
}

Template.loading.onRendered(function(event) {
    if (!Session.get('loadingSplash')) {
        this.loading = window.pleaseWait({
            logo: '/images/logoing.png',
            backgroundColor: '#337ab7',
            loadingHtml: message + spinner
        });
        Session.set('loadingSplash', true); // just show loading splash once
    }
});

Template.loading.onDestroyed(function(event) {
    if (this.loading) {
        this.loading.finish();
    }
});


var message = '<p class="loading-message"></p>';
var spinner = ' <div class="sk-spinner sk-spinner-chasing-dots"><div class="sk-dot1"></div></div>';