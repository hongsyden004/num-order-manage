import Swal from 'sweetalert2';
import { Template } from 'meteor/templating';
import './photo_profile.html';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
})

Template.photo_profile.events({
    'click .icon-post-back': (event) => {
        history.go(-1);
    },
    'click #choose-img': (event) => {
        $(".file-upload").click();
    },
    'change .file-upload': function(event) {
        readURLImages($(event.target));
    },
});


Template.photo_profile.onRendered(function () {
   
}); 

function readURLImages (input){
    if (input[0].files && input[0].files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.btn-cancel').show();
            $('.btn-use-image').show();
            $('#img-info-crop').show();

            var w_viewport = 200; h_viewport = 200; w_boundary = 230; h_boundary = 230;

            showCropImage(w_viewport, h_viewport, w_boundary, h_boundary, e.target.result);           
        }
        reader.readAsDataURL(input[0].files[0]);
    }
}

function showCropImage(w_viewport, h_viewport, w_boundary, h_boundary, base64){
    $('#edit-pic').croppie('destroy');
    var profilePic = $('#edit-pic').croppie({
        viewport: {
            width: w_viewport,
            height: h_viewport
        },
        boundary: {
            width: w_boundary,
            height: h_boundary
        }
    });
    profilePic.croppie('bind', {
        url: base64,
    });
}


