import { Meteor } from 'meteor/meteor';
import '../imports/collections/partner.js';
import '../imports/collections/province';
import '../imports/collections/unit';
import '../imports/collections/items';
import '../imports/common/user.js';
import '../imports/common/tabular.js';

import './user';

import { Partners } from '../imports/collections/partner.js';
import { Province } from '../imports/collections/province.js';
import { Unit } from '../imports/collections/unit';
import { Category } from '../imports/collections/category.js';
import { Items } from '../imports/collections/items';

Meteor.publish('userData', function(userid) {
    return Meteor.users.find({ _id: userid });
});

Meteor.publish('userListAllByFilter', function(filter) {
    return Partners.find(filter);
});

Meteor.publish('signedinPartner', function() {
    return Partners.find({ account: this.userId });
});

Meteor.publish('PublishPartnerById', function(id) {
    return Partners.find({ _id: id });
});

Meteor.publish('PublishUserLogin', function(userid) {
    return Partners.find({ account: userid });
});

Meteor.publish('getItemById', function(itemId) {
    return Items.find({_id: itemId});
})

Meteor.publish('getAllCategory', function(){
    return Category.find({});
})

Meteor.publish('getAllUnit', function(){
    return Unit.find({});
})

Meteor.startup(() => {
    Meteor.publish(null, function() {
        if (this.userId) {
            return Meteor.roleAssignment.find({ 'user._id': this.userId });
        } else {
            this.ready()
        }
    })
});

Meteor.methods({
    DeleteProvince: function(id) {
        if (!Roles.userIsInRole(this.userId, ['admin'])) return false;
        var result = Province.remove({ _id: id });
        if(result) {
            return true;
        } else {
            return false;
        }
    },
    DeleteUnit: function (id) {
        var result = Unit.remove({ _id: id });
        if(result) {
            return true;
        } else {
            return false;
        }
    },
    DeleteCategory: function (id) { 
        var result = Category.remove({_id: id});
        if(result) {
            return true;
        } else {
            return false;
        }
    },
    updateIsView: function(id) {
        var rsCate = Category.findOne({_id: id});
        var resultIsView = null;
        if(rsCate.isview == true) 
        {
            resultIsView = false;
        } else {
            resultIsView = true;
        }
        var result = Category.update({_id: id}, {$set: {isview: resultIsView} });
        if(result) {
            return true;
        } else {
            return false;
        }
    },
    DeleteItem: function(id){
        var result = Items.remove({_id: id});
        if(result) {
            return true;
        } else {
            return false;
        }
    }
})