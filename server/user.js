Meteor.startup(function() {
    //Add defaul user roles
    var roles = ['admin', 'employee', 'employer'];
    // this will fail if the roles package isn't installed
    if (Meteor.roles.find().count() === 0) {
        roles.map(function(role) {
            Roles.createRole(role)
        })
    };
});

Meteor.methods({
    setRoleForExternal: function(role) {
        var checkrole = Roles.userIsInRole(this.userId, ['admin', 'employee', 'employer']);
        if (checkrole === false) {
            if (role === "employee") {
                Roles.setUserRoles(this.userId, 'admin');
            } else if (role == 'employer') {
                Roles.setUserRoles(this.userId, 'employer');
            }
        }
    }
})